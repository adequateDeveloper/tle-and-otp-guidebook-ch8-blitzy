defmodule Blitzy.Caller do

  # The mechanism to aggregate the return results of all the workers to calculate the average time taken for all
  # successful requests.
  
  
  def start(n_workers, url) do
    caller = self # if self is passed in the subsequent spawn call, then self will refer to the newly spawned process, and
                  # not the calling process.
    1..n_workers
      |> Enum.map(fn _ignore -> spawn(fn -> Blitzy.Worker.start(url, caller)  end)  end)  # results in a list of worker pids
      |> Enum.map(fn _ignore ->
           receive do                               # set up a mailbox to receive the response from each of the workers
             {_pid, {:ok, result}} -> result
             _otherwise -> nil                      # toss both {:error, reason} and {:error, :unknown} results
           end
         end)
  end

end