defmodule Blitzy.Supervisor do
  use Supervisor


  ## API

  def start_link(:ok) do
    Supervisor.start_link(__MODULE__, :ok)
  end


  ## Callbacks

  def init(:ok) do
    children = [
      supervisor(Task.Supervisor, [[name: Blitzy.TasksSupervisor]])
    ]

    supervise(children, [strategy: :one_for_one])
  end

end