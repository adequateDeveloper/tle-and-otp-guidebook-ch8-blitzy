use Mix.Config

defmodule Blitzy.CLI do
  alias Blitzy.{TasksSupervisor, Worker}
  require Logger

  # required function by escript configuration in mix.exs
  def main(args) do

    # Read the configuration from config/config.exs.
    # First start the master node in distributed mode. Then connect to the slave nodes.
    # Then pass the list of all the nodes in the cluster to process_options/2
    # From config: :"a@127.0.0.1"
    Application.get_env(:blitzy, :master_node) |> Node.start

    # From config: [:"b@127.0.0.1", :"c@127.0.0.1", :"d@127.0.0.1"]
    Application.get_env(:blitzy, :slave_nodes) |> Enum.each(&Node.connect(&1))

    # A tokenized list of arguments.
    # Given: $ ./blitzy -n 100 http://www.google.com
    # Results: ["-n", "100", "http://google.com"]
    args
      |> parse_args
      |> process_options([node|Node.list])
  end

  defp parse_args(args) do
    # First alias --requests to n. This is a way to specify shorthand for switches.
    # OptionParser expects all switches to start with --<switch>, and single-character switches -<switch> need to be
    # appropriately aliased.
    # Invalid: OptionParser.parse(["-n", "100"]) ==> {[], [], [{"-n", "100"}]}
    # Valid: OptionParser.parse(["--n", "100"])  ==> {[n: "100"], [], []}
    OptionParser.parse(args, aliases: [n: :requests], strict: [requests: :integer])
  end

  defp process_options(options, nodes) do
    case options do
      # --requests or -n contains a single integer value
      # there is a url value
      # there are no invalid arguments
      {[requests: n], [url], []} -> do_requests(n, url, nodes)
      _otherwise -> do_help
    end
  end

  defp do_requests(n_requests, url, nodes) do
    Logger.info "Pummelling #{url} with #{n_requests} requests"

    total_nodes  = Enum.count(nodes)
    req_per_node = div(n_requests, total_nodes)

    nodes
    |> Enum.flat_map(fn node ->
         1..req_per_node |> Enum.map(fn _ ->
           Task.Supervisor.async({TasksSupervisor, node}, Worker, :start, [url])
         end)
       end)

    |> Enum.map(&Task.await(&1, :infinity))
    |> parse_results
    |> report_results
  end

  defp parse_results(results) do
    {successes, _failures} =
      results
        |> Enum.partition(fn result ->
             case result do
                {:ok, _time} -> true
                _otherwise   -> false
             end
           end)

    {:ok, results, successes}
  end

  defp report_results({:ok, results, successes}) do
    total_workers = Enum.count(results)
    total_success = Enum.count(successes)
    total_failure = total_workers - total_success

    data = successes |> Enum.map(fn {:ok, time} -> time end)
    average_time = average(data)
    longest_time = Enum.max(data)
    shortest_time = Enum.min(data)

    IO.puts """
    Total workers    : #{total_workers}
    Successful reqs  : #{total_success}
    Failed res       : #{total_failure}
    Average (msecs)  : #{average_time}
    Longest (msecs)  : #{longest_time}
    Shortest (msecs) : #{shortest_time}
    """
  end

  defp average(list) do
    sum = Enum.sum(list)
    if sum > 0 do
      sum / Enum.count(list)
    else
      0
    end
  end

  defp do_help do
    IO.puts """
    Usage:
    blitzy -n [requests] [url]

    Options:
    -n, [--requests]        # Number of requests

    Example:
    $ ./blitzy -n 100 http://www.google.com
    """
    System.halt 0
  end

end