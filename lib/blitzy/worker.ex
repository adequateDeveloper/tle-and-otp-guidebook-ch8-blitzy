defmodule Blitzy.Worker do
  use Timex
  require Logger

  # The worker fetches the web page and computes how long the request takes.
  # The worker then sends its results to the caller

  # Called by Blitzy.run/2
  def start(url, func \\ &HTTPoison.get/1) do
    Logger.info "Running on #node-#{node}"
    {timestamp, response} = Duration.measure(fn -> func.(url) end)
    handle_response({Duration.to_milliseconds(timestamp), response})
  end

  # Called by Blitzy.Caller.start/2 (non-Task)
#  def start(url, caller, func \\ &HTTPoison.get/1) do
#    {timestamp, response} = Duration.measure(fn -> func.(url) end)
#    caller |> send({self, handle_response({Duration.to_milliseconds(timestamp), response})})
#  end

  defp handle_response({msecs, {:ok, %HTTPoison.Response{status_code: code}}}) when code >= 200 and code <= 304 do
    Logger.info "worker [#{node}-#{inspect self}] completed in #{msecs} msecs"
    {:ok, msecs}
  end

  defp handle_response({_msecs, {:error, reason}}) do
     Logger.info "worker [#{node}-#{inspect self}] error due to #{inspect reason}"
    {:error, reason}
  end

  defp handle_response({_msecs, _response}) do
     Logger.info "worker [#{node}-#{inspect self}] errored out"
    {:error, :unknown}
  end

end