defmodule Blitzy do
  use Application

# A distributed load tester.
# The load tester creates a barrage of GET requests to an end point and measures the response time.
# Because there’s a limit to the number of open network connections a single physical machine can make,
# this will be a distributed system.
# In this case, the number of web requests needed is spread evenly across each node in the cluster.
#
# an example of unleashing Blitzy on an unsuspecting victim:
#
# % ./blitzy -n 100 http://www.bieberfever.com
# [info] Pummeling http://www.bieberfever.com with 100 requests
#
# This example creates 100 workers, each of which makes an HTTP GET request to www.bieberfever.com; you then measure the
# response time and count the number of successful requests. Behind the scenes, Blitzy creates a cluster and splits the
# workers across the nodes in the cluster. In this case, 100 workers are split across 4 nodes.
# Therefore, 25 workers are running on each node.
#
# The number of requests is split across the available nodes in the cluster. Once a node has received results from all
# of its workers, the respective node reports back to the worker.
# Once all the workers from each individual node have finished, the result is sent over the master node.
# The master node then aggregates and reports the results:
#
#   Total workers    : 1000
#   Successful reqs  : 1000
#   Failed res       : 0
#   Average (msecs)  : 3103.478963
#   Longest (msecs)  : 5883.235
#   Shortest (msecs) : 25.061


  # Start the top-level supervisor, which will then start the rest of the supervision tree.
  def start(_type, _args) do
    Blitzy.Supervisor.start_link(:ok)
  end

  # Convenience function to run Blitzy workers in Tasks
  def run(n_workers, url) when is_integer(n_workers) and n_workers > 0 and is_binary(url) do
    worker_fun = fn -> Blitzy.Worker.start(url) end

    1..n_workers
      |> Enum.map(fn _ignore -> Task.async(worker_fun) end)   # asyncronously execute the Worker.start/2 Task in the background
      |> Enum.map(&Task.await(&1, :infinity))                 # block until all the Tasks return their results
      |> parse_results                                        # [{:ok, time}, ...]
      |> report_results                                       # {:ok, [{:ok, time}, ...], [{:ok, time}, ...]}

    :ok
  end

  defp parse_results(results) do
    {successes, _failures} =
      results
        |> Enum.partition(fn result ->
             case result do
                {:ok, _time} -> true
                _otherwise   -> false
             end
           end)

    {:ok, results, successes}
  end

  defp report_results({:ok, results, successes}) do
    total_workers = Enum.count(results)
    total_success = Enum.count(successes)
    total_failure = total_workers - total_success

    data = successes |> Enum.map(fn {:ok, time} -> time end)
    average_time = average(data)
    longest_time = Enum.max(data)
    shortest_time = Enum.min(data)

    IO.puts """
    Total workers    : #{total_workers}
    Successful reqs  : #{total_success}
    Failed res       : #{total_failure}
    Average (msecs)  : #{average_time}
    Longest (msecs)  : #{longest_time}
    Shortest (msecs) : #{shortest_time}
    """
  end

  defp average(list) do
    sum = Enum.sum(list)
    if sum > 0 do
      sum / Enum.count(list)
    else
      0
    end
  end

end
